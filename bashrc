## modify ##

# Base
PS1='\u-\A-\W $ '
set -o vi
echo "OTHER_WRITABLE 01;34" >> $HOME/.dircolors # disable directory highlight.
alias ax="chmod a+x"
alias dx="chmod a-x"
alias lspwd='ls | sed "s:^:`pwd`/:"'

# cd
alias cd..='cd ..'
alias ..="cd .."
alias ...="cd ..; cd .."

# vim
alias vi=vim

# Git
alias gs="git status"
alias gc="git commit ."
alias gfo="git fetch origin"
alias gmm="git merge origin/master"
alias gmd="git merge origin/develop"
alias gcm="git checkout master"
alias gpm="git push origin master"
alias gcd="git checkout develop"
alias gpd="git push origin develop"
alias gtm='foo(){ git archive master --prefix="$1/" | gzip > $1.tar.gz; }; foo '
alias groot="while [ ! -d .git ]; do cd ..; done"   # Go to the root of a git repo.