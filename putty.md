
https://www.putty.org/

### 终端
Terminal/Keyboard Xterm R6

### 窗口
Window Lines of scrollback 20000

### 字体
Window/Appearance Font Setting Consolas 四号

### 配色(desert)
/Window/Colours
- Default Foreground: 255/255/255
- Default Background: 51/51/51
- ANSI Black: 77/77/77
- ANSI Green: 152/251/152
- ANSI Yellow: 240/230/140
- ANSI Blue: 205/133/63
- ANSI Blue Bold: 135/206/235
- ANSI Magenta: 255/222/173 or 205/92/92
- ANSI Cyan: 255/160/160
- ANSI Cyan Bold: 255/215/0
- ANSI White: 245/222/179
