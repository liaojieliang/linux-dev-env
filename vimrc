
" Colors
syntax enable       " enable syntax highlight


" Leader Shortcuts
let mapleader=","
" jk is escape, type fast.
inoremap jk <esc>


" UI
set number          " show line numbers
set wildmenu        " visual autocomplete for command menu
set showmatch       " highlight matching [{()}]
set lazyredraw      " redraw only when we need to

filetype indent on  " load filetype-specific indent files

set cursorline      " Hightlight the current line.
:highlight CursorLine   cterm=NONE ctermbg=black guibg=black
:highlight CursorColumn cterm=NONE ctermbg=black guibg=black
:nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>


" Space and tab
set tabstop=2       " number of visual spaces per TAB
set softtabstop=2   " number of spaces in tab when editing
set shiftwidth=2    " affects  >>, << or == and automatic indentation
set expandtab       " tabs are spaces
set autoindent


" Search
set incsearch       " search as characters are entered
set hlsearch        " highlight matches
set ignorecase      " Case insensitive
" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>


" Folding
set foldenable          " enable folding
set foldlevelstart=2    " open most folds by default
set foldminlines=3      " 10 nested fold max
set foldmethod=indent   " fold based on indent level
" use space to open/closes folds
nnoremap <space> za


" Movement
" move vertically by visual line
nnoremap j gj
nnoremap k gk
" highlight last inserted text
nnoremap gV `[v`]
